import { HdLoadingDialog } from '../../common/components/HdLoading'
import { HdSearch } from '../../common/components/HdSearch'
import { request } from '../../common/utils/request'
import { QuestionItem } from '../Home/QuestionItemComp'
import { IQuestionList } from '../Home/QuestionListComp'
import { InterviewItemComp } from './InterviewItemComp'

// 热门企业实体
interface Company {
  icon: ResourceStr
  count: number
}

interface TabItem {
  name: string
  value?: number
  onAreaChange?: (area: Area) => void
}

export interface InterviewItem {
  id: string
  stem: string
  content: string
  likeCount: number
  views: number
  creatorName: string
  creatorAvatar: ResourceStr
  createdAt: string
  tags: string[]
  likeFlag: 0 | 1 | null
}


@Component
export struct Interview {
  async aboutToAppear(){
    await this.loadInterview(false)
  }
  @StorageProp("topHeight") topHeight: number = 0
  LoadingController:CustomDialogController=new CustomDialogController({
    builder:HdLoadingDialog({message:'加载中...'}),
    customStyle:true
  })
  @State
  left: number = 16
  pageId=0
  @State
  sort: number | undefined = 30
  @State isRefreshing:boolean=true
  @State isRequesting:boolean=false

  @State
  list: QuestionItem[] = []

  private async loadInterview(isOnLoadMore:boolean) {
    if(isOnLoadMore){
      this.isRequesting=true
      this.pageId++

    }else{
      this.pageId=1
    }
    this.LoadingController.open()
    const res = await request<IQuestionList>({
      url: 'hm/question/list',
      params: {
        questionBankType: 9,
        page: this.pageId,
        sort: this.sort
      }
    })
    this.LoadingController.close()
    if(isOnLoadMore){
      this.list.push(...res.data.data.rows)
      this.isRequesting=false
    }
    else{
      this.list=res.data.data.rows
      this.isRefreshing=false
    }
  }
  setLineLeft(area: Area) {
    const width = area.width as number
    const x = area.position.x as number
    this.left = x + (width - 16) / 2
  }

  // 热门企业
  @Builder
  companyBuilder(com: Company) {
    Column({ space: 12 }) {
      Image(com.icon)
        .width(36)
        .aspectRatio(1)
        .margin({ top: 20 })
        .objectFit(ImageFit.Contain)
      Text() {
        Span('热门指数')
        Span(`${com.count}K`)
          .fontColor($r('app.color.common_green'))
      }
      .fontSize(13)
      .fontColor($r('app.color.common_gray_01'))

      Button({ type: ButtonType.Normal }) {
        Text('查看')
          .fontSize(14)
      }
      .backgroundColor($r('app.color.common_gray_bg'))
      .padding(0)
      .height(30)
      .width(80)
      .borderRadius(8)

    }
    .backgroundColor(Color.White)
    .borderRadius(8)
    .height(140)
    .width(100)
  }

  // 标签页
  @Builder
  tabBuilder(tab: TabItem) {
    Text(tab.name)
      .fontSize(14)
      .padding(10)
      .fontColor(this.sort === tab.value ? '#000' : '#979797')
      .animation({
        duration: 300
      })
      .onClick((e) => {
        this.setLineLeft(e.target.area)
        this.sort = tab.value
        this.loadInterview(false)
      })
      .onAreaChange((_o, n) => tab.onAreaChange && tab.onAreaChange(n))
  }

  build() {
    Column() {
      Row({ space: 16 }) {
        Image($r('app.media.ic_interview_text'))
          .width(54)
          .height(20)
          .objectFit(ImageFit.Contain)
          .layoutWeight(1)
        HdSearch()
          .layoutWeight(3)
      }
      .padding({ left: 16, right: 16 })
      .height(64)

      // 热门企业
      Scroll() {
        Row({ space: 10 }) {
          this.companyBuilder({ icon: $r('app.media.ic_company_hw'), count: 1.5 })
          this.companyBuilder({ icon: $r('app.media.ic_company_wr'), count: 2.9 })
          this.companyBuilder({ icon: $r('app.media.ic_company_gg'), count: 3.1 })
          this.companyBuilder({ icon: $r('app.media.ic_company_zj'), count: 5.3 })
          this.companyBuilder({ icon: $r('app.media.ic_company_wy'), count: 3.6 })
        }
      }
      .width('100%')
      .height(160)
      .scrollable(ScrollDirection.Horizontal)
      .scrollBar(BarState.Off)
      .padding(10)

      .edgeEffect(EdgeEffect.Spring)
      .backgroundColor($r('app.color.common_gray_bg'))

      // 标签页
      Row() {
        Row() {
          this.tabBuilder({
            name: '推荐', value: 30,
            onAreaChange: (area) => {
              this.setLineLeft(area)
            }
          })
          this.tabBuilder({ name: '最新', value: undefined })
        }
        .width('100%')
        .padding({ left: 16, right: 16 })
        .height(40)
        .border({
          width: { bottom: $r('app.float.common_border_width') },
          color: $r('app.color.common_gray_border')
        })

        Text()
          .width(16)
          .height(2)
          .backgroundColor('#000000')
          .position({ x: this.left, y: 38 })
          .animation({
            duration: 300
          })
      }

      // 文章列表
      Refresh({refreshing:$$this.isRefreshing}) {
        List() {
          ForEach(this.list, (item: InterviewItem, index: number) => {
            ListItem() {
              InterviewItemComp({ item })
                .padding(16)
            }
          })
        }
        .layoutWeight(1)
        .padding({ bottom: 50 })
        .onReachEnd(()=>{
          //页面数量小于一页时，防止触底多次
          if(this.list.length>0&&this.list.length<10){
            return
          }
          if(!this.isRequesting){
            this.loadInterview(true)
          }
        })
      }
      .onRefreshing(async ()=>{
        await this.loadInterview(false)
        this.isRefreshing=false
      })
    }
    .width('100%')
    .height('100%')
    .padding({ top: this.topHeight })
  }
}