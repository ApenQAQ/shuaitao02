import { HdLoadingDialog } from '../../common/components/HdLoading';
import { request } from '../../common/utils/request'
import { FilterParams } from './QuestionFilterComp';
import { QuestionItem, QuestionItemComp } from './QuestionItemComp'
import { router } from '@kit.ArkUI';

/**
 * 报文数据
 */
export interface IQuestionList {
  /**
   * 总页数
   */
  pageTotal: number;
  /**
   * 数据集合
   */
  rows: QuestionItem[];
  /**
   * 总数
   */
  total: number;
}

@Component
export struct QuestionListComp {
  @State questionList:QuestionItem[]=[]
  @State isRefreshing:boolean=true
  @State isRequesting:boolean=false
  pageId=0
  @Prop typeId:number=0
  @Prop project:boolean=false
  @Prop selfIndex:number=0
  @Prop keyword:string=''
  @Prop
  @Watch('onSort')
  params:FilterParams=new FilterParams()
  onSort(){
    this.onLoadMore(false)
  }
  LoadingController:CustomDialogController=new CustomDialogController({
    builder:HdLoadingDialog({message:'加载中...'}),
    customStyle:true
  })
  // 优化代码-合并下拉刷新和加载更多到一个函数中
  async onLoadMore(isOnLoadMore:boolean) {
    //如果list自己的index与watch监听到的(也就是切换后的)index不符合，不进行请求
    if(this.selfIndex!==this.params.index&&!this.project){
      return
    }
    if(isOnLoadMore){
      this.isRequesting=true
      this.pageId++

    }else{
      this.pageId=1
    }
    this.LoadingController.open()
    const res = await request<IQuestionList>({
      url: 'hm/question/list',
      params: {
        type: this.typeId,
        questionBankType: 10,
        page:this.pageId,
        sort:this.params.sort,
        keyword:this.keyword
      }
    });
    this.LoadingController.close()
    if(isOnLoadMore){
      this.questionList.push(...res.data.data.rows)
      this.isRequesting=false
    }
    else{
      this.questionList=res.data.data.rows
      this.isRefreshing=false
    }
    AppStorage.setOrCreate('listId',this.questionList.map(item=>item.id))
  }
  build() {
    Refresh({refreshing:$$this.isRefreshing}) {
      List() {
        ForEach(this.questionList, (item:QuestionItem) => {
          ListItem(){
            QuestionItemComp({
              item: item
            })
              .padding({left:10,right:10})
          }
          .onClick(()=>{
            router.pushUrl({
              url:'pages/QuestionDetailPage',
              params:{
                id:item.id
              }
            })
          })
        })
      }
      //触底加载更多
      .onReachEnd(()=>{
        //页面数量小于一页时，防止触底多次
        if(this.questionList.length>0&&this.questionList.length<10){
          return
        }
        if(!this.isRequesting){
          this.onLoadMore(true)
        }
      })
    }
    // 上拉刷新
    .onRefreshing(async ()=>{
      await this.onLoadMore(false)
      this.isRefreshing=false
    })
  }
}